# Config files for the Defuse Game
# By Erik Hentell
# Based on the BASIC code by Creative Computing
# Morristown, New Jersey, found at
# atariarchives.org

config = {
    "title": "Defuse!\nInitially created by Creative Computing, Morristown, New Jersey\nPython Code by Erik Hentell, 2021",
    "copyright": "\nInitial BASIC code is the property of Creative Computing\nThe Python code for this current iteration is public domain",
    "instructions": "An experimental building has been created that is 100 rooms wide, 100 rooms long, and 100 floors, each with the same layout. That's 1 million rooms!\n\nUnfortunately, a component in this building has gone haywire and is going to detonate, taking the building with it!\n\nNot to worry, though! We have given you a drone to go through the building, find this component, and deactivate it. Unfortunately, we don't have the building schematics, so you will have to guide the drone remotely through the various rooms and floors. You can do this by sending the drone into various rooms, by indicating each room according to Length, Width and Level. This means, to check a certain room, you would tell the drone to go 4 rooms down, 8 rooms across, on level 12, or (4, 8, 12). \n\nThe drone has sensors to detect if it is getting closer to or further from the component. Using this, you can get the drone where it needs to be, but remember: you only have 200 seconds before the component breaks down and detonates! Good luck!",
    "success": "\n\nSuccess! The component was found and neutralized in {{REPLACE}} seconds! Congratulations!",
    "failure": "\n\nThe explosion cracks the foundations and support of the building, bringing one of the biggest and most expensive buildings to the ground in a matter of minutes! Oh, well... you tried anyway..."
}

