# Environment class for the Defuse Game
# By Erik Hentell
# Based on the BASIC code by Creative Computing
# Morristown, New Jersey, found at
# atariarchives.org

# ---- IMPORTS ----

import config

# ---- CLASSES ----

class DefuseFall: 
    def __init__(self, exitfloor):
        self.varyfloor = exitfloor - 1
        self.failtext = config.config["failure"]

    def fallalert(floorlevel):
        print("You just sent the drone out the window of floor {thefloor}!".format(thefloor=floorlevel))

    def fallending():
        print("\nThe drone just crashed into the ground! There's no way to neutralize the component!")
        print("\n\nA few moments later...\n\n")
        print(self.failtext)

    def fallprint(fallingfloor):
            print("\nThe drone just fell past floor {newfloor}!".format(newfloor=fallingfloor))

    def fallloop(self):
        fallalert(self.exitfloor)

        while (self.varyfloor > 0):
            fallprint(self.varyfloor)

            self.varyfloor = self.varyfloor - 1

        fallending()

    def read(self):
        self.fallloop(self)
