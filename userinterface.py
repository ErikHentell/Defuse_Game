# UI class for the Defuse Game
# By Erik Hentell
# Based on the BASIC code by Creative Computing
# Morristown, New Jersey, found at
# atariarchives.org

# ---- IMPORTS ----

import os
from random import *
import config

# ---- OBJECTS ----

class collectedrows:
    self.rowset = []
    self.timecode = 0

    def addset(dataset):
        self.timecode = self.timecode + 10

        dataset["sec"] = self.timecode

        self.rowset.append(dataset)

    def giveset():
        return self.rowset

class columnui:
    self.rows = collectedrows()

    def displaycolhead():
        print("{0:<15} {1:<5} {2:<5} {3:<5} {4:<15} {5:<15}".format("Signal", "L", "W", "H", "Seconds", "Coordindates(L,W,H)"))

    def displayrow(self):
        for x in self.collectedrows.giveset():
            print("{0:<15} {1:<5} {2:<5} {3:<5} {4:<15} {5:<15}\n".format(x["sig"], x["l"], x["w"], x["h"], x["sec"], x["coord"]))

class utilityui:
    def clearscreen():
        os.system('cls' if os.name == 'nt' else 'clear')

    def screenspacer():
        print("\n\n")

    def titleandcopy(self):
        self.clearscreen()

        print(config.config["title"])
        print(config.config["copyright"])
        
        self.screenspacer()

    def instructtext(self):
        print(config.config["instructions"])

        self.screenspacer()

    def getcoordinates(self):
        self.screenspacer()

        datadict = { "version": 1, "l": 0, "w": 0, "h": 0 }

        print("Please enter a number for the following coordinates.\nThese numbers will identify the room to check.\n")

        datadict["l"] = int(input("The number of a room along the length of the building (l): "))
        datadict["w"] = int(input("The number of a room along the width of the building (w): "))
        datadict["h"] = int(input("The floor to search of the building (h): ")) 

        return datadict

    def read(self, options):
        if(options["version"] == 1):
            if(options["args"] == "getcoords"):
                resultdata = self.getcoordinates(self)

                return resultdata

# ---- FUNCTIONS ----

