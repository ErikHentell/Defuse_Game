# Defuse_Game

Defuse is a command-line game initially made by Creative Computing in 1975. The code was published in the book "More BASIC Computer Games" in 1975, and can be found here as a scan of the pages: [https://www.atariarchives.org/morebasicgames/showpage.php?page=48](https://www.atariarchives.org/morebasicgames/showpage.php?page=48).

The code in this repository is an attempt to translate the 1975 BASIC code to modern Python and, possibly, other languages (such as JavaScript) at a later date.
