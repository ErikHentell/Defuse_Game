# component class for the Defuse Game
# By Erik Hentell
# Based on the BASIC code by Creative Computing
# Morristown, New Jersey, found at
# atariarchives.org

# ---- IMPORTS ----


# ---- CLASS ----

class hiddencomponent:
    self.locationdict = { "version": 1, "sig": 0, "sec": 0, "l": 0, "w": 0, "h": 0 }

    def __init__(self):
        self.locationdict["l"] = randint(0,100)
        self.locationdict["w"] = randint(0,100)
        self.locationdict["h"] = randint(0,100)

    def updateseconds(self):
        self.locationdict["sec"] = self.locationdict["sec"] + 10

    def updatesignal(self, newsig):
        self.locationdict["sig"] = newsig
        self.updateseconds()

    def read(self, option):
        if(option["version"] == 1):
            if(option["argument"] == "location"):
                resultdict = { "version": 1, "l": self.locationdict["l"], "w": self.locationdict["w"], "h": self.locationdict["h"] }

                return resultdict

            if(option["argument"] == "signal"):
                lwh = option["argdata"]

                self.updatesignal( (1000 - abs(((self.locationdict["l"] / 100 + self.locationdict["w"] + self.locationdict["h"]) * 100) - ((lwh["l"] / 100 + lwh["w"] + lwh["h"]) * 100))) )

                resultdict = { "version": 1, "sig": self.locationdict["sig"] }
                return resultdict

            if(option["argument"] == "seconds"):
                resultdict = { "version": 1, "sec": self.locationdict["sec"] }

                return resultdict
