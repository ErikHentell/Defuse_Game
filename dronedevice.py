# drone class for the Defuse Game
# By Erik Hentell
# Based on the BASIC code by Creative Computing
# Morristown, New Jersey, found at
# atariarchives.org

# ---- IMPORTS ----

# ---- CLASSES ----

class dronedevice:
    self.resultdict= { "version": 1, "l": 0, "w": 0, "h": 0 }

    def read(self):
        return self.resultdict

    def write(self, coordset):
        if (coordset["version"] == 1):
            self.resultdict["l"] = coordset["l"]
            self.resultdict["w"] = coordset["w"]
            self.resultdict["h"] = coordset["h"]

            if ((self.resultdict["l"] > 99) or (self.resultdict["w"] > 99) or (self.resultdict["h"] > 99):
                return { "version": 1, "error": 0 }
            else:
                return { "version": 1, "error": 1 }
